package edu.uci.graphQ;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Kai Wang
 *
 * Created by Aug 27, 2014
 */
public class GraphQFileNames {
	
	public static String getFileNameInDegreeData(String baseFileName, int numShards, int numSubPartition) {
		return baseFileName + "." + numShards + "." + numSubPartition + ".indegree";
	}
	
	public static String getFileNameOutDegreeData(String baseFileName, int numShards, int numSubPartition) {
		return baseFileName + "." + numShards + "." + numSubPartition + ".outdegree";
	}
	
	public static String getFileNameAbstractionGraph(String baseFileName, int numShards, int numSubPartition) {
		return baseFileName + "." + numShards + "." + numSubPartition + ".ag";
	}
	
	public static String getFileNameAbstractVertexData(String baseFileName, int numShard, int numSubPartition) {
		return baseFileName + "." + numShard + "." + numSubPartition + ".abvdata";
	}
	
	public static String getFileNameEDataOffset(String baseFileName, int numShard) {
		return baseFileName + "." + numShard + ".edataOffset";
	}
	
	/**
	 * load abstract in degree data.
	 * @return ArrayList<Integer>
	 * @throws IOException 
	 */
	public static ArrayList<Integer> loadAbstractInDegreeData(String baseFilename, 
			int nShards, int numSubPartition) throws IOException {
        
        String degreeFileName = GraphQFileNames.getFileNameInDegreeData(baseFilename, nShards, numSubPartition);
        BufferedReader rd = new BufferedReader(new FileReader(new File(degreeFileName)));
        String ln;
        ArrayList<Integer> inDegrees = new ArrayList<Integer>(nShards * numSubPartition);
        while((ln = rd.readLine()) != null) {
        	int degree = Integer.parseInt(ln);
        	inDegrees.add(degree);
        }
        
		return inDegrees;
	}
}
