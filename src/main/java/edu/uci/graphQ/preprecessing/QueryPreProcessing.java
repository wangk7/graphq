package edu.uci.graphQ.preprecessing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import com.yammer.metrics.core.TimerContext;

import edu.cmu.graphchi.ChiFilenames;
import edu.cmu.graphchi.ChiLogger;
import edu.cmu.graphchi.ChiVertex;
import edu.cmu.graphchi.apps.Pagerank;
import edu.cmu.graphchi.datablocks.FloatConverter;
import edu.cmu.graphchi.engine.GraphChiEngine;
import edu.cmu.graphchi.engine.VertexInterval;
import edu.cmu.graphchi.engine.auxdata.VertexData;
import edu.cmu.graphchi.shards.MemoryShard;
import edu.cmu.graphchi.shards.SlidingShard;
import edu.uci.graphQ.GraphQFileNames;
import edu.uci.graphQ.GraphQProgram;
import edu.uci.graphQ.abstraction.AbstractEdge;
import edu.uci.graphQ.abstraction.AbstractGraph;
import edu.uci.graphQ.abstraction.AbstractVertex;
import edu.uci.graphQ.data.SubInterval;

/**
 * @author Kai Wang
 *
 * Created by Aug 21, 2014
 */
public class QueryPreProcessing <VertexDataType, EdgeDataType> extends GraphChiEngine<VertexDataType, EdgeDataType> {
	private int intervalLength;
	private AbstractGraph ag = null;
	private ArrayList<AbstractVertex> abVertices = null;
	private Map<VertexRange, Integer> concreteIdToAbstractId = null;
	private AbstractVertexIdTranslate abTranslate = null;
	private boolean[][] addAbstractInEdgesHelper = null;
	private boolean[][] addAbstractOutEdgesHelper = null;
	private int[] indegree = null;
	private int[] outdegree = null;
	private int subIntervalStart, subIntervalEnd;																																																																																																																																																
	private int numAbstractVertices;																																																																																																																																																																																																																																																																																																								
	private int maxWindow = 20000000;
	private int nSubPartitions = 0;
	
	private static final Logger logger = ChiLogger.getLogger("graphQ preprocessing");
	
	public QueryPreProcessing(String baseFilename, int nShards, int nSubPartitions) throws FileNotFoundException, IOException {
		super(baseFilename, nShards);
		this.nSubPartitions = nSubPartitions;
		concreteIdToAbstractId = new HashMap<VertexRange, Integer>();
		numAbstractVertices = nShards * nSubPartitions;
		addAbstractInEdgesHelper = new boolean[numAbstractVertices][numAbstractVertices];
		addAbstractOutEdgesHelper = new boolean[numAbstractVertices][numAbstractVertices];
		indegree = new int[numAbstractVertices];
		outdegree = new int[numAbstractVertices];
	}
	
	/**
	 * load graphchi intervals from disk
	 */
	public void loadIntervals() throws FileNotFoundException, IOException {
        intervals = ChiFilenames.loadIntervals(baseFilename, nShards);
        this.intervalLength = intervals.get(0).getLastVertex() - intervals.get(0).getFirstVertex() + 1;
    } 
	
	/**
	 * add in edges for every abstract vertex.
	 * @param vertices 
	 */
	public void addEdges(ChiVertex<VertexDataType, EdgeDataType>[] vertices) {
		if(vertices == null)
			return;
		
		for(int i = 0; i < vertices.length; i ++ ) {
			ChiVertex<VertexDataType, EdgeDataType> v = vertices[i];
			int id = v.getId();
			
			// in edges
			for(int j = 0; j < v.numInEdges(); j ++) {
				int inEdgeId = v.getInEdgeId(j);
				
				// get abstract vertex id
				int abstractDst = abTranslate.forward(id);
				int abstractSrc = abTranslate.forward(inEdgeId);
				
				if(abstractDst == abstractSrc)
					continue;
				// check add abstract in edges helper
				if(!addAbstractInEdgesHelper[abstractDst][abstractSrc]) {
					abVertices.get(abstractDst).addInEdge(abstractSrc);
					
					//set flag to true
					addAbstractInEdgesHelper[abstractDst][abstractSrc] = true;
				}
			}
			
			// no need to add out edges.
			// out edges
//			for(int j = 0; j < v.numOutEdges(); j ++) {
//				int outEdgeId = v.getOutEdgeId(j);
//				
//				// get abstract vertex id
//				int abstractSrc = abTranslate.forward(id);
//				int abstractDst = abTranslate.forward(outEdgeId);
//				
//				if(abstractDst == abstractSrc)
//					continue;
//				// check add abstract out edges helper
//				if(!addAbstractOutEdgesHelper[abstractSrc][abstractDst]) {
//					abVertices.get(abstractSrc).addOutEdge(abstractDst);
//					
//					//set flag to true
//					addAbstractOutEdgesHelper[abstractSrc][abstractDst] = true;
//				}
//			}
			
		}
		
	}

	/**
	 * create abstract vertices.
	 */
	private void createAbstractVertices() {
		ag = new AbstractGraph(numAbstractVertices);
		
		int count = 0;
		
		for(int i = 0; i < nShards; i++) {
			VertexInterval interval = intervals.get(i);
			int intervalSt = interval.getFirstVertex();
			int intervalEd = interval.getLastVertex();
			if(intervalLength < nSubPartitions)
				throw new IllegalArgumentException("num of sub partition illegal.");
			
			int subLen = intervalLength / nSubPartitions + 1;
			int subIntervalSt = intervalSt;
			for(int j = 0; j < nSubPartitions; j++) {
				int subIntervalEd = subIntervalSt + subLen - 1;
				if(subIntervalEd >= intervalEd)
					subIntervalEd = intervalEd;
				
				int in = indegree[count];
				int out = outdegree[count];
				AbstractVertex v = new AbstractVertex(count, subIntervalSt, subIntervalEd, i, j, in ,out);
				ag.addVertex(v);
				
				subIntervalSt = subIntervalEd + 1;
				
				VertexRange range = new VertexRange(subIntervalSt, subIntervalEd);
				if(!concreteIdToAbstractId.containsKey(range))
					concreteIdToAbstractId.put(range, count);
				count ++;
			}
		}
		
		abVertices = ag.getVertices();
	}

	/**
	 * preprocessing for graphQ.
	 * 
	 * 1. compute abstract degree
	 * 2. create abstract vertices.
	 * 3. add abstract in and out edges.
	 */
	public void process( ) throws FileNotFoundException, IOException {
		logger.info("start to do preprocessing.");
		logger.info("construct abstraction graph...");
		
		this.loadIntervals();
		
		if(intervals == null)
			throw new IllegalArgumentException("intervals null.");
		
		int intervalLength = intervals.get(0).getLastVertex() - intervals.get(0).getFirstVertex() + 1;
		abTranslate = new AbstractVertexIdTranslate(nShards, nSubPartitions, intervals, intervalLength);
		
		int nprocs = 4;
        if (Runtime.getRuntime().availableProcessors() > nprocs) {
            nprocs = Runtime.getRuntime().availableProcessors();
        }

        if (System.getProperty("num_threads") != null)
            nprocs = Integer.parseInt(System.getProperty("num_threads"));

        logger.info(":::::::: Using " + nprocs + " execution threads :::::::::");

        parallelExecutor = Executors.newFixedThreadPool(nprocs);
        loadingExecutor = Executors.newFixedThreadPool(4);
        
		long startTime = System.currentTimeMillis();
		
		logger.info("vertex data handler:: " + vertexDataConverter);
		/* Initialize vertex-data handler */
        if (vertexDataConverter != null) {
        	logger.info("vertex data handler!");
            vertexDataHandler = new VertexData<VertexDataType>(numVertices(), baseFilename, vertexDataConverter, true);
            vertexDataHandler.setBlockManager(blockManager);
        }

        blockManager.reset();
        
        // load for computing abstract vertex degree.
        for(int execInterval=0; execInterval < nShards; ++execInterval) {
            int intervalSt = intervals.get(execInterval).getFirstVertex();
            int intervalEn = intervals.get(execInterval).getLastVertex();

            logger.info((System.currentTimeMillis() - startTime) * 0.001 + " interval: " + intervalSt + " -- " + intervalEn);
            MemoryShard<EdgeDataType> memoryShard = createMemoryShard(intervalSt, intervalEn, execInterval);
            
            subIntervalStart = intervalSt;

            while (subIntervalStart <= intervalEn) {
                int adjMaxWindow = maxWindow;
                if (Integer.MAX_VALUE - subIntervalStart < maxWindow) adjMaxWindow = Integer.MAX_VALUE - subIntervalStart - 1;

                if (anyVertexScheduled(subIntervalStart, Math.min(intervalEn, subIntervalStart + adjMaxWindow ))) {
                    ChiVertex<VertexDataType, EdgeDataType>[] vertices = null;
                    
                    subIntervalEnd = determineNextWindow(subIntervalStart, Math.min(intervalEn, subIntervalStart + adjMaxWindow ));
                    int nvertices = subIntervalEnd - subIntervalStart + 1;

                    logger.info("Subinterval:: " + subIntervalStart + " -- " + subIntervalEnd);

                    vertices = new ChiVertex[nvertices];
                    logger.info("Init vertices...");
                    initVertices(nvertices, subIntervalStart, vertices);

                    logger.info("Loading for computing abstract degree...");
                    long t0 = System.currentTimeMillis();
                    this.loadBeforeUpdates(execInterval, vertices, memoryShard, subIntervalStart, subIntervalEnd);
                    logger.info("Load took: " + (System.currentTimeMillis() - t0) + "ms");
                    
                    // compute degree for abstract vertices
                    computeDegree(vertices);
                }
                
                subIntervalStart = subIntervalEnd + 1;
            }
            
            memoryShard.release();
		}
        
        // write abstract in degree to file
        writeInDegreeData();
        
        // write abstract out degree to file
        writeOutDegreeData();
        
        //set helper to false.
        resetHelper();
      	
        // create abstract vertices
     	createAbstractVertices();
     	
     	// write vertex data to file
     	writeVertexData();
     	
     	// load for adding abstract edges.
     	for(int execInterval=0; execInterval < nShards; ++execInterval) {
             int intervalSt = intervals.get(execInterval).getFirstVertex();
             int intervalEn = intervals.get(execInterval).getLastVertex();

             logger.info((System.currentTimeMillis() - startTime) * 0.001 + " interval: " + intervalSt + " -- " + intervalEn);
             MemoryShard<EdgeDataType> memoryShard = createMemoryShard(intervalSt, intervalEn, execInterval);
             
             subIntervalStart = intervalSt;

             while (subIntervalStart <= intervalEn) {
                 int adjMaxWindow = maxWindow;
                 if (Integer.MAX_VALUE - subIntervalStart < maxWindow) adjMaxWindow = Integer.MAX_VALUE - subIntervalStart - 1;

                 if (anyVertexScheduled(subIntervalStart, Math.min(intervalEn, subIntervalStart + adjMaxWindow ))) {
                     ChiVertex<VertexDataType, EdgeDataType>[] vertices = null;
                     
                     subIntervalEnd = determineNextWindow(subIntervalStart, Math.min(intervalEn, subIntervalStart + adjMaxWindow ));
                     int nvertices = subIntervalEnd - subIntervalStart + 1;

                     logger.info("Subinterval:: " + subIntervalStart + " -- " + subIntervalEnd);

                     vertices = new ChiVertex[nvertices];
                     logger.info("Init vertices...");
                     initVertices(nvertices, subIntervalStart, vertices);

                     logger.info("Loading for adding abstract edges...");
                     long t0 = System.currentTimeMillis();
                     loadBeforeUpdates(execInterval, vertices, memoryShard, subIntervalStart, subIntervalEnd);
                     logger.info("Load took: " + (System.currentTimeMillis() - t0) + "ms");
                     
                     // add abstract edges.
                     addEdges(vertices);
                 }
                 
                 subIntervalStart = subIntervalEnd + 1;
             }
             
             memoryShard.release();
 		}       
     	
     	// write abstract graph to file
     	writeAG();
     	
     	addAbstractInEdgesHelper = null;
	}
	

	/**
	 * write vertex data to file
	 * @throws IOException 
	 */
	private void writeVertexData() throws IOException {
		FileWriter wr = new FileWriter(GraphQFileNames.getFileNameAbstractVertexData(baseFilename, nShards, nSubPartitions));
		
		for(int i = 0; i < numAbstractVertices; i ++) {
			AbstractVertex v = abVertices.get(i);
			int start = v.getConcreteIdStart();
			int end = v.getConcreteIdEnd();
			int partitionId = v.getPartitionId();
			int subPartitionId = v.getSubPartitionId();
			wr.write(start + " " + end + " " + partitionId + " " + subPartitionId + "\n");
		}
		
		wr.close();
	}

	/**
	 * set helper array to false
	 */
	private void resetHelper() {
		
		for(int i = 0; i < numAbstractVertices; i++ ) {
      		for(int j = 0; j < numAbstractVertices; j++) {
      			addAbstractInEdgesHelper[i][j] = false;
//      	    addAbstractOutEdgesHelper[i][j] = false;
      		}
      	}
		
		addAbstractOutEdgesHelper = null;
	}

	/**
	 * write abstract graph edge list to file
	 * @throws IOException 
	 */
	private void writeAG() throws IOException {
		FileWriter wr = new FileWriter(GraphQFileNames.getFileNameAbstractionGraph(baseFilename, nShards, nSubPartitions));
		
		// write in edges for each abstract vertex
		for(int i = 0; i < numAbstractVertices; i ++) {
			AbstractVertex v = abVertices.get(i);
			if(v.numInEdges() > 0) {
				for(int j = 0; j < v.numInEdges(); j ++) {
					int dst = v.getAbstractId();
					int src = v.getInEdgeId(j);
					wr.write(src + " " + dst + "\n");
				}
			}
		}
		
		wr.close();
	}

	/**
	 * write abstract in degree to file
	 * @throws IOException 
	 */
	private void writeInDegreeData() throws IOException {
		FileWriter wr = new FileWriter(GraphQFileNames.getFileNameInDegreeData(baseFilename, nShards, nSubPartitions));
		
		for(int i = 0; i < numAbstractVertices; i ++) 
			wr.write(indegree[i] + "\n");
		
		wr.close();
	}
	
	/**
	 * write abstract out degree to file
	 * @throws IOException 
	 */
	private void writeOutDegreeData() throws IOException {
		FileWriter wr = new FileWriter(GraphQFileNames.getFileNameOutDegreeData(baseFilename, nShards, nSubPartitions));
		
		for(int i = 0; i < numAbstractVertices; i ++) 
			wr.write(outdegree[i] + "\n");
		
		wr.close();
	}
	
	/**
	 * compute in and out degree for abstract vertices
	 * @param: ChiVertice[] vertices
	 */
	private void computeDegree(
			ChiVertex<VertexDataType, EdgeDataType>[] vertices) {
		if(vertices == null)
			return;
		
		for(int i = 0; i < vertices.length; i ++ ) {
			ChiVertex<VertexDataType, EdgeDataType> v = vertices[i];
			int id = v.getId();
			
			// in edges
			for(int j = 0; j < v.numInEdges(); j ++) {
				int inEdgeId = v.getInEdgeId(j);
				
				// get abstract vertex id
				int abstractDst = abTranslate.forward(id);
				int abstractSrc = abTranslate.forward(inEdgeId);

				if(abstractDst == abstractSrc)
					continue;
				// check add abstract in edges helper
				if(!addAbstractInEdgesHelper[abstractDst][abstractSrc]) {
					indegree[abstractDst] ++;
					
					//set flag to true
					addAbstractInEdgesHelper[abstractDst][abstractSrc] = true;
				}
				
				// check add abstract out edges helper
				if(!addAbstractOutEdgesHelper[abstractSrc][abstractDst]) {
					outdegree[abstractSrc] ++;
					
					//set flag to true
					addAbstractOutEdgesHelper[abstractSrc][abstractDst] = true;
				}
				
			}
			
		}
	}
	
	@Override
	public void loadBeforeUpdates(int interval, final ChiVertex<VertexDataType, EdgeDataType>[] vertices,  final MemoryShard<EdgeDataType> memShard,
	            final int startVertex, final int endVertex) throws IOException {
	    final Object terminationLock = new Object();
	    final TimerContext _timer = loadTimer.time();
	    // TODO: make easier to read
	    synchronized (terminationLock) {
        	
        	final AtomicInteger countDown = new AtomicInteger(1);

            if (!disableInEdges) {
                try {

                    logger.info("Memshard: " + startVertex + " -- " + endVertex);
                    memShard.loadVertices(startVertex, endVertex, vertices, disableOutEdges, parallelExecutor);
                    logger.info("Loading memory-shard finished." + Thread.currentThread().getName());

                    if (countDown.decrementAndGet() == 0) {
                        synchronized (terminationLock) {
                            terminationLock.notifyAll();
                        }
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    throw new RuntimeException(ioe);
                }  catch (Exception err) {
                    err.printStackTrace();
                }
            }
            
            // barrier
            try {
                while(countDown.get() > 0) {
                    terminationLock.wait(5000);
                    if (countDown.get() > 0) {
                        logger.info("Still waiting for loading, counter is: " + countDown.get());
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
        }
	    _timer.stop();
	
	}

	class VertexRange {
		int start;
		int end;
		
		public VertexRange(int start, int end) {
			this.start = start;
			this.end = end;
		}
		
		public int getStart() {
			return start;
		}
		
		public int getEnd() {
			return end;
		}
		
	}
}
