package edu.uci.graphQ.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import com.yammer.metrics.core.TimerContext;

import edu.cmu.graphchi.ChiLogger;
import edu.cmu.graphchi.ChiVertex;
import edu.cmu.graphchi.engine.GraphChiEngine;
import edu.cmu.graphchi.engine.VertexInterval;
import edu.cmu.graphchi.engine.auxdata.VertexData;
import edu.cmu.graphchi.shards.MemoryShard;
import edu.cmu.graphchi.shards.SlidingShard;
import edu.uci.graphQ.GraphQConfig;
import edu.uci.graphQ.GraphQFileNames;
import edu.uci.graphQ.GraphQProgram;
import edu.uci.graphQ.abstraction.AbstractVertex;

/**
 * @author Kai Wang
 *
 * Created by Aug 28, 2014
 */
public class GraphQEngine <VertexDataType, EdgeDataType> extends GraphChiEngine<VertexDataType, EdgeDataType> {
	private int numAbstractVertices;
	private int numSubPartition = 0;
	private ArrayList<AbstractVertex> abVertices = null;
	private MemoryShard<EdgeDataType> memoryShard = null;
	private static ArrayList<Integer> curListofPartitions = null;
	private ArrayList< ArrayList<Integer> > listsofCandidatePartitions = null;
	private ArrayList<Integer> graphQslidingShards = null;
	private int[][] edataOffset = null;
	private int[][] adjOffset = null;
	private int[][] curvid = null;
	public static boolean IN_COMBINE_MODE = false;
	private static final float memBudget = 0.5f;
	
	private static final Logger logger = ChiLogger.getLogger("graphQ engine");
	
	public GraphQEngine(String baseFileName, int numShards, int numSubPartition) throws FileNotFoundException, IOException {
		super(baseFileName, numShards);
		this.numSubPartition = numSubPartition;
		numAbstractVertices = numShards * numSubPartition;
		abVertices = new ArrayList<AbstractVertex>(numAbstractVertices);
		curListofPartitions = new ArrayList<Integer>();
		listsofCandidatePartitions = new ArrayList<ArrayList<Integer> >();
		graphQslidingShards = new ArrayList<Integer>();
		edataOffset = new int[nShards][nShards];
		adjOffset = new int[nShards][nShards];
		curvid = new int[nShards][nShards];
	}
	
	/**
	 * run graphQ
	 * @throws IOException 
	 */
	public void run(GraphQProgram<VertexDataType, EdgeDataType> program) throws IOException {
		logger.info("Running GraphQ...");
		logger.info("Loading abstraction graph...");
		ArrayList<Integer> indegrees = loadAbstractInDegreeData();
		ArrayList<Integer> outdegrees = loadAbstractOutDegreeData();
		loadVertexData(indegrees, outdegrees);
		loadAG();
		logger.info("Loading abstraction graph finish.");
		
		loadEdataOffset();
		
		int nprocs = 4;
        if (Runtime.getRuntime().availableProcessors() > nprocs) {
            nprocs = Runtime.getRuntime().availableProcessors();
        }

        if (System.getProperty("num_threads") != null)
            nprocs = Integer.parseInt(System.getProperty("num_threads"));

        logger.info(":::::::: Using " + nprocs + " execution threads :::::::::");

        parallelExecutor = Executors.newFixedThreadPool(nprocs);
        loadingExecutor = Executors.newFixedThreadPool(4);
        
        chiContext.setIteration(1);
        
		initializeSlidingShards();
		
		logger.info("vertex data handler:: " + vertexDataConverter);
		/* Initialize vertex-data handler */
        if (vertexDataConverter != null) {
        	logger.info("vertex data handler!");
            vertexDataHandler = new VertexData<VertexDataType>(numVertices(), baseFilename, vertexDataConverter, true);
            vertexDataHandler.setBlockManager(blockManager);
        }

        blockManager.reset();
		
        boolean isAnswered = false;
		
		// 2. start to do refine and combine
		logger.info("finish computing initial partitions, start to do refine and combine.");
		
//		for(int iter = 0; iter < 4; iter++) {
			
//			chiContext.setIteration(iter);
		while(true) {
			// 2.1 if list is empty, call refine to return a list of list of candidate partitions.
			if(listsofCandidatePartitions.isEmpty()) {
				logger.info("candidate partition is empty. start to do refine.");
				listsofCandidatePartitions = program.refine(abVertices);
				logger.info("refine finished.");
				curListofPartitions = listsofCandidatePartitions.get(0);
				logger.info("curlist : " + curListofPartitions);
			} else { // not empty
				
				logger.info("ready to do combine.");
				// 2.2 get current list of candidate partitions
				curListofPartitions = listsofCandidatePartitions.get(0);
				logger.info("curlist : " + curListofPartitions);
				if(curListofPartitions.size() < 2)
					throw new RuntimeException("candidate list of partitions < 2");
				int size = curListofPartitions.size();
				
				// 2.3 check memory availability
				if(isMemAvailable(size)) {
					
					// 2.4 load candidate partitions to combine
					for(int i = 0; i < size; i++) {
						// get candidate partition id
						int execInterval = curListofPartitions.get(i);
						VertexInterval interval = intervals.get(execInterval);
						int intervalSt = interval.getFirstVertex();
						int intervalEn = interval.getLastVertex();
						int intervalLen = intervalEn - intervalSt + 1;
						
						ChiVertex<VertexDataType, EdgeDataType>[] vertices = new ChiVertex[intervalLen];
						// get sliding shards
						getGraphQSlidingShards(execInterval);
						
						// in combine mode, no need to add all in edges for memory partition.
						GraphQConfig.setCombineMode();
						
						// load candidate partition and do computation
						isAnswered = loadAndUpdates(program, execInterval, intervalSt, intervalEn, vertices);
						if(isAnswered)
							return;
						
						// write result back and release blocks
						memoryShard.commitAndRelease(modifiesInedges, modifiesOutedges);
						graphQslidingShards.clear();
						
					}
					
					for(SlidingShard shard : slidingShards) 
		                shard.flush();
						
					listsofCandidatePartitions.remove(0);
					
				} else { // memory is not available
					logger.info("exceeds memory budget, query is NOT answered.");
					return;
				}
				
				
			}
		}
	}

	/** get graphq slidingshards
	 * @param: int execInterval
	 * @return: void
	 */
	private void getGraphQSlidingShards(int execInterval) {
		for(int i = 0; i < curListofPartitions.size(); i++) {
			int id = curListofPartitions.get(i);
			if(id != execInterval)
				graphQslidingShards.add(id);
		}
	}

	/**
	 * check if there is enough memory to run.
	 * @param: size
	 * @return: boolean
	 */
	private boolean isMemAvailable(int size) {
		if(size <= memBudget * nShards)
			return true;
		else
			return false;
	}

	/**
	 * load and update partition
	 * @param program 
	 * @param: int id, ChiVertex [] vertices
	 * @throws IOException 
	 */
	public boolean loadAndUpdates(GraphQProgram<VertexDataType, EdgeDataType> program, int execInterval, int intervalSt, int intervalEn, 
			ChiVertex<VertexDataType, EdgeDataType>[] vertices ) throws IOException {
		logger.info("loading partition " + execInterval);
		long startTime = System.currentTimeMillis();
		logger.info((System.currentTimeMillis() - startTime) * 0.001 + " interval: " + intervalSt + " -- " + intervalEn);
		slidingShards.get(execInterval).flush(); 
		memoryShard = createMemoryShard(intervalSt, intervalEn, execInterval);
		memoryShard.setIntervals(intervals);
		memoryShard.setCandidatePartitions(curListofPartitions);
        
        subIntervalStart = intervalSt;
        int vertexBlockId = -1;
        while (subIntervalStart <= intervalEn) {
            int adjMaxWindow = maxWindow;
            if (Integer.MAX_VALUE - subIntervalStart < maxWindow) adjMaxWindow = Integer.MAX_VALUE - subIntervalStart - 1;
            
            
            if (anyVertexScheduled(subIntervalStart, Math.min(intervalEn, subIntervalStart + adjMaxWindow ))) {
                subIntervalEnd = determineNextWindow(subIntervalStart, Math.min(intervalEn, subIntervalStart + adjMaxWindow ));
                int nvertices = subIntervalEnd - subIntervalStart + 1;
                
                logger.info("Subinterval:: " + subIntervalStart + " -- " + subIntervalEnd);
                logger.info("Init vertices...");
                vertexBlockId = initVertices(nvertices, subIntervalStart, vertices);

                long t0 = System.currentTimeMillis();
                loadBeforeUpdates(execInterval, vertices, memoryShard, subIntervalStart, subIntervalEnd);
                logger.info("Load took: " + (System.currentTimeMillis() - t0) + "ms");
                
            }
            
            long t1 = System.currentTimeMillis();
            
            execUpdates(program, vertices);
            logger.info("Update exec: " + (System.currentTimeMillis() - t1) + " ms.");
            
            boolean isAnswered = false;
            // check if query is answered.
			isAnswered = program.check(vertices);
			if(isAnswered) {
				logger.info("query is answered. ");
				return true;
			}

            // Write vertices (async)
            final int _firstVertex = subIntervalStart;
            final int _blockId = vertexBlockId;
            parallelExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        vertexDataHandler.releaseAndCommit(_firstVertex, _blockId);
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            });
            
            subIntervalStart = subIntervalEnd + 1;
        }
        
        return false;
	}

	/**
	 * load abstraction graph
	 * @throws IOException 
	 */
	public void loadAG() throws IOException {
		
		String agFileName = GraphQFileNames.getFileNameAbstractionGraph(baseFilename, nShards, numSubPartition);
		BufferedReader rd = new BufferedReader(new FileReader(new File(agFileName)));
		String ln;
		long lineNum = 0;
		while((ln = rd.readLine()) != null) {
			lineNum ++;
			if (lineNum % 2000000 == 0) logger.info("Reading line: " + lineNum);
			
			String[] tok = ln.split(" ");
			if(tok.length == 2) {
				int src = Integer.parseInt(tok[0]);
				int dest = Integer.parseInt(tok[1]);
				
				AbstractVertex srcVertex = abVertices.get(src);
				AbstractVertex destVertex = abVertices.get(dest);
				srcVertex.addOutEdge(dest);
				destVertex.addInEdge(src);
				
			} else {
				throw new IllegalArgumentException("load AG : tok length is not 2.");
			}
		}
		
	}

	/**
	 * load abstract vertex data
	 * @param: indegrees, outdegrees
	 * @throws IOException 
	 */
	public void loadVertexData(ArrayList<Integer> indegrees,
			ArrayList<Integer> outdegrees) throws IOException {
		
		String vertexFileName = GraphQFileNames.getFileNameAbstractVertexData(baseFilename, nShards, numSubPartition);
		BufferedReader rd = new BufferedReader(new FileReader(new File(vertexFileName)));
		String ln;
		int count = 0;
		while((ln = rd.readLine()) != null) {
			String[] tok = ln.split(" ");
			if(tok.length == 4) {
				int start = Integer.parseInt(tok[0]);
				int end = Integer.parseInt(tok[1]);
				int partionId = Integer.parseInt(tok[2]);
				int subPartionId = Integer.parseInt(tok[3]);
				
				AbstractVertex v = new AbstractVertex(count, start, end, partionId, 
						subPartionId, indegrees.get(count), outdegrees.get(count));
				abVertices.add(v);
				count ++;
				
			} else {
				throw new IllegalArgumentException("load vertex data : tok length is not 4.");
			}
			
		}
		
	}

	/**
	 * load abstract in degree data.
	 * @return ArrayList<Integer>
	 * @throws IOException 
	 */
	public ArrayList<Integer> loadAbstractInDegreeData() throws IOException {
        
        String degreeFileName = GraphQFileNames.getFileNameInDegreeData(baseFilename, nShards, numSubPartition);
        BufferedReader rd = new BufferedReader(new FileReader(new File(degreeFileName)));
        String ln;
        ArrayList<Integer> inDegrees = new ArrayList<Integer>(nShards * numSubPartition);
        while((ln = rd.readLine()) != null) {
        	int degree = Integer.parseInt(ln);
        	inDegrees.add(degree);
        }
        
		return inDegrees;
	}
	
	/**
	 * load abstract out degree data.
	 * @return ArrayList<Integer>
	 * @throws IOException 
	 */
	public ArrayList<Integer> loadAbstractOutDegreeData() throws IOException {
        
        String degreeFileName = GraphQFileNames.getFileNameOutDegreeData(baseFilename, nShards, numSubPartition);
        
        BufferedReader rd = new BufferedReader(new FileReader(new File(degreeFileName)));
        String ln;
        ArrayList<Integer> outDegrees = new ArrayList<Integer>(nShards * numSubPartition);
        while((ln = rd.readLine()) != null) {
        	int degree = Integer.parseInt(ln);
        	outDegrees.add(degree);
        }
        
		return outDegrees;
	}
	
	/**
	 * load edata offset of sliding shard
	 * @throws IOException 
	 */
	public void loadEdataOffset( ) throws IOException {
		
		String offsetFileName = GraphQFileNames.getFileNameEDataOffset(baseFilename, nShards);
		BufferedReader rd = new BufferedReader(new FileReader(new File(offsetFileName)));
		String ln;
		while((ln = rd.readLine()) != null) {
			String[] tok = ln.split(" ");
			if(tok.length == 5) {
				int i = Integer.parseInt(tok[0]);
				int j = Integer.parseInt(tok[1]);
				int adj = Integer.parseInt(tok[2]);
				int id = Integer.parseInt(tok[3]);
				int edge = Integer.parseInt(tok[4]);
				adjOffset[i][j] = adj;
				curvid[i][j] = id;
				edataOffset[i][j] = edge;
				
			} else {
				throw new IllegalArgumentException("load edata offset : tok length is not 5.");
			}
			
		}
		
	}
	
	@Override
	public void loadBeforeUpdates(int interval, final ChiVertex<VertexDataType, EdgeDataType>[] vertices,  final MemoryShard<EdgeDataType> memShard,
	            final int startVertex, final int endVertex) throws IOException {
	    final Object terminationLock = new Object();
	    final TimerContext _timer = loadTimer.time();
	    // TODO: make easier to read
	    synchronized (terminationLock) {
	    	final AtomicInteger countDown = new AtomicInteger(1);
	
	        if (!disableInEdges) {
	            try {
	
	                logger.info("Memshard  " + interval + " : "+ startVertex + " -- " + endVertex);
	                memShard.loadVertices(startVertex, endVertex, vertices, disableOutEdges, parallelExecutor);
	                logger.info("Loading memory-shard finished." + Thread.currentThread().getName());
	
	                if (countDown.decrementAndGet() == 0) {
	                    synchronized (terminationLock) {
	                        terminationLock.notifyAll();
	                    }
	                }
	            } catch (IOException ioe) {
	                ioe.printStackTrace();
	                throw new RuntimeException(ioe);
	            }  catch (Exception err) {
	                err.printStackTrace();
	            }
	        }
	        
            if (!disableOutEdges) {
                for(int i = 0; i < graphQslidingShards.size(); i++) {
                	int p = graphQslidingShards.get(i);
                    final SlidingShard<EdgeDataType> shard = slidingShards.get(p);
                    
                    // set sliding shard offset according to edata offset during preprocessing.
                    shard.setOffset(adjOffset[interval][p], curvid[interval][p], edataOffset[interval][p]);
                    
                    try {
                        shard.readNextVertices(vertices, startVertex, false);
                        if (countDown.decrementAndGet() == 0) {
                            synchronized (terminationLock) {
                                terminationLock.notifyAll();
                            }
                        }

                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                        throw new RuntimeException(ioe);
                    }  catch (Exception err) {
                        err.printStackTrace();
                    }
                }
            }
	
	         //barrier
	        try {
	            while(countDown.get() > 0) {
	                terminationLock.wait(5000);
	                if (countDown.get() > 0) {
	                    logger.info("Still waiting for loading, counter is: " + countDown.get());
	                }
	            }
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	        
	    }
	    _timer.stop();
	
	}
	
}
