package edu.uci.graphQ.abstraction;


/**
 * @author Kai Wang
 *
 * Created by Aug 20, 2014
 */
public class AbstractVertex {
	private int abstractId;
	private int concreteIdStart, concreteIdEnd;
	private int partitionId;
	private int subPartitionId;
	private int numInEdges, numOutEdges;
//	private ArrayList<AbstractEdge> inEdges = null;
//	private ArrayList<AbstractEdge> outEdges = null;
	private int inEdgeArray[] = null;
	private int outEdgeArray[] = null;
	private int curInEdgeIndex, curOutEdgeIndex;
	
	public AbstractVertex(int abstractId, int concreteIdStart, 
			int concreteIdEnd, int partitionId, int subPartitionId, int indegree, int outdegree) {
		this.abstractId = abstractId;
		this.concreteIdStart = concreteIdStart;
		this.concreteIdEnd = concreteIdEnd;
		this.partitionId = partitionId;
		this.subPartitionId = subPartitionId;
//		inEdges = new ArrayList<AbstractEdge>();
//		outEdges = new ArrayList<AbstractEdge>();
		inEdgeArray = new int[indegree];
		outEdgeArray = new int[outdegree];
		curInEdgeIndex = 0;
		curOutEdgeIndex = 0;
		this.numInEdges = indegree;
		this.numOutEdges = outdegree;
	}
	
	public int getAbstractId() {
		return abstractId;
	}
	
	public int getConcreteIdStart() {
		return concreteIdStart;
	}
	
	public int getConcreteIdEnd() {
		return concreteIdEnd;
	}
	
	public int getPartitionId() {
		return partitionId;
	}
	
	public int getSubPartitionId() {
		return subPartitionId;
	}
	
	public void addInEdge(AbstractEdge e) {
		if(e == null)
			throw new IllegalArgumentException("abstracte edge null.");
		int vertexId = e.getFrom();
		inEdgeArray[curInEdgeIndex ++] = vertexId;
	}
	
	public void addInEdge(int from) {
		inEdgeArray[curInEdgeIndex ++] = from;
	}
	
	public void addOutEdge(AbstractEdge e) {
		if(e == null)
			throw new IllegalArgumentException("abstracte edge null.");
		
		int vertexId = e.getTo();
		outEdgeArray[curOutEdgeIndex ++] = vertexId;
	}
	
	public void addOutEdge(int to) {
		outEdgeArray[curOutEdgeIndex ++] = to;
	}
	
	public int numInEdges() {
		return numInEdges;
	}
	
	public int numOutEdges() {
		return numOutEdges;
	}
	
	public int[] getInEdgeArray() {
		return inEdgeArray;
	}
	
	public int[] getOutEdgeArray() {
		return outEdgeArray;
	}
	
	public int getInEdgeId(int i) {
		return inEdgeArray[i];
	}
	
	public int getOutEdgeId(int i) {
		return outEdgeArray[i];
	}
		
	public String toString() {
			
		return abstractId + " : " + concreteIdStart + " -- " + concreteIdEnd
				+ " interval id : " + partitionId + " subinterval id : " + subPartitionId
				+ " --> indegree : " + numInEdges + " outdegree : " + numOutEdges ; 
	}
}
