package edu.uci.graphQ.abstraction;

import java.util.ArrayList;

/**
 * @author Kai Wang
 *
 * Created by Aug 20, 2014
 */
public class AbstractGraph {
	private ArrayList<AbstractVertex> vertices = null;
	
	public AbstractGraph(int numVertices) {
		this.vertices = new ArrayList<AbstractVertex>(numVertices);
	}
	
	public void addVertex(AbstractVertex v) {
		if(v == null) 
			throw new IllegalArgumentException("abstract vertex null.");
		
		int id = v.getAbstractId();
		vertices.add(id, v);
	}
	
	public void addVertex(int abstractId, int concreteIdStart, 
			int concreteIdEnd, int partitionId, int subPartitionId, int indegree, int outdegree) {
		AbstractVertex v = new AbstractVertex(abstractId, concreteIdStart,concreteIdEnd, 
				partitionId, subPartitionId, indegree, outdegree);
	}
	
	public ArrayList<AbstractVertex> getVertices() {
		return vertices;
	}
	
	public void addEdge(AbstractEdge e) {
		if(e == null)
			throw new IllegalArgumentException("abstract edge null.");
		
		int from = e.getFrom();
		int to = e.getTo();
		AbstractVertex src = vertices.get(from);
		AbstractVertex dst = vertices.get(to);
		src.addOutEdge(e);
		dst.addInEdge(e);
	}
	
	public void addEdge(int from, int to, int weight) {
		AbstractEdge e = new AbstractEdge(from, to, weight);
		AbstractVertex src = vertices.get(from);
		AbstractVertex dst = vertices.get(to);
		src.addOutEdge(e);
		dst.addInEdge(e);
	}
	
	// needs to implement efficiently
	public static boolean isReachable(AbstractVertex from, AbstractVertex to) {
		if(from == null || to == null)
			throw new IllegalArgumentException("abstract vertex null.");
		
		if(from.numOutEdges() > 0) {
			for(int i = 0; i < from.numOutEdges(); i ++ ) {
				int dest = from.getOutEdgeId(i);
				if(to.getAbstractId() == dest)
					return true;
			}
		}
		
		return false;
	} 
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for(AbstractVertex v : vertices) {
			sb.append(v.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
}
