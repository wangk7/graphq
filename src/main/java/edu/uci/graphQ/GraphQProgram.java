package edu.uci.graphQ;

import java.util.ArrayList;

import edu.cmu.graphchi.ChiVertex;
import edu.cmu.graphchi.GraphChiProgram;
import edu.uci.graphQ.abstraction.AbstractEdge;
import edu.uci.graphQ.abstraction.AbstractVertex;

/**
 * @author Kai Wang
 *
 * Created by Aug 22, 2014
 */

/**
 * All GraphQ applications must extend this class.
 * @param <VertexDataType>
 * @param <EdgeDataType>
 */
public interface GraphQProgram <VertexDataType, EdgeDataType> extends GraphChiProgram<VertexDataType, EdgeDataType> {

	/**
	 * initial partitions.
	 * @param numShards 
	 * @return:ArrayList<Integer>
	 */
	public ArrayList<Integer> initialPartition(int numShards);

	/**
	 * check if query is answered.
	 * @param: vertices
	 * @return: boolean
	 */
	public boolean check(ChiVertex<VertexDataType, EdgeDataType>[] vertices);

	/**
	 * check AG to give a list of candidate partitions to be combined.
	 * @param abVertices 
	 * @return: a list of candidate partitions
	 */
	public ArrayList<ArrayList<Integer>> refine(ArrayList<AbstractVertex> abVertices);

}
