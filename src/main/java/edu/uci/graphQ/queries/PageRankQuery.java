package edu.uci.graphQ.queries;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Logger;

import edu.cmu.graphchi.ChiFilenames;
import edu.cmu.graphchi.ChiLogger;
import edu.cmu.graphchi.ChiVertex;
import edu.cmu.graphchi.GraphChiContext;
import edu.cmu.graphchi.apps.Pagerank;
import edu.cmu.graphchi.datablocks.FloatConverter;
import edu.cmu.graphchi.engine.GraphChiEngine;
import edu.cmu.graphchi.io.CompressedIO;
import edu.cmu.graphchi.preprocessing.EdgeProcessor;
import edu.cmu.graphchi.preprocessing.FastSharder;
import edu.cmu.graphchi.preprocessing.VertexIdTranslate;
import edu.cmu.graphchi.preprocessing.VertexProcessor;
import edu.cmu.graphchi.util.IdFloat;
import edu.cmu.graphchi.util.Toplist;
import edu.uci.graphQ.GraphQFileNames;
import edu.uci.graphQ.GraphQProgram;
import edu.uci.graphQ.abstraction.AbstractVertex;
import edu.uci.graphQ.engine.GraphQEngine;
import edu.uci.graphQ.preprecessing.QueryPreProcessing;

/**
 * @author Kai Wang
 *
 * Created by Aug 20, 2014
 */
public class PageRankQuery extends Pagerank implements GraphQProgram <Float, Float> {
	
	private static Logger logger = ChiLogger.getLogger("pagerankquery");
	private static final float thresholdValue = 2400.00000001f;
	private static int numShards = 0;
	private static int numSubPartition = 0;
	private static ChiVertex targetVertex = null;
	private static ArrayList<Integer> curPartitionList = null;
	private static HashMap<Integer, Integer> map = null;
	
	public ArrayList<Integer> initialPartition(int numShards) {
		return null;
	}
	
	public boolean check(ChiVertex<Float, Float>[] vertices) {
		if(vertices != null) {
			for(int i = 0; i < vertices.length; i++) {
				if(vertices[i].getValue() > thresholdValue) {
					targetVertex = vertices[i];
					logger.info("vertex id : " + targetVertex.getId() + " value : " + vertices[i].getValue());
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * refine logic : favor abstract vertex with high in degree and high out degree.
	 * 
	 * */
	public ArrayList<ArrayList<Integer>> refine(ArrayList<AbstractVertex> absVertices) {
		if(absVertices == null)
			return null;
		
		int [] priorityPartition = new int[numShards];
		boolean [] priorityTag = new boolean[numShards];
		ArrayList<ArrayList<Integer>> listOflist = new ArrayList<ArrayList<Integer>>();
		
		if(curPartitionList == null) {
			// degree = indegree + outdegree
			HashMap<Integer, Integer> degreeMap = new HashMap<Integer, Integer>(2000);
			for(int index = 0; index < absVertices.size(); index++) {
				int degree = 0;
				AbstractVertex v = absVertices.get(index);
				degree = v.numInEdges() + v.numOutEdges();
				degreeMap.put(v.getPartitionId() * numSubPartition + v.getSubPartitionId(), degree);
			}
			
			// sort abstract vertex by degree
			ArrayList<Map.Entry<Integer, Integer>> degreeMapList = new ArrayList<Map.Entry<Integer, Integer>>(degreeMap.entrySet());
			Collections.sort(degreeMapList, new Comparator<Map.Entry<Integer, Integer>>(){
				
				public int compare(Entry<Integer, Integer> o1, 
						Entry<Integer, Integer> o2) {
					return o2.getValue() - o1.getValue();
				}
			});
			
			// generate priority partition list
			int index = 0;
			for(int i = 0; i < degreeMapList.size(); i++) {
				Map.Entry<Integer, Integer> entry = degreeMapList.get(i);
				int partitionId = entry.getKey() / numSubPartition;
				if(!priorityTag[partitionId]) {
					priorityPartition[index++] = partitionId;
					priorityTag[partitionId] = true;
				}
				if(index >= numShards)
					break;
			}
			
			int count = 0;
			int step = 10;
			while(count < priorityPartition.length) {
				ArrayList<Integer> list = new ArrayList<Integer>();
				count += step;
				for(int j = 0; j < count; j++) 
					list.add(priorityPartition[j]);
						
				Collections.sort(list);
				logger.info("list : " + list);
				listOflist.add(list);
				logger.info("listoflist : " + listOflist);
					
			}
		}
		
		return listOflist;
	}
	
	@Override
	public void update(ChiVertex<Float, Float> vertex, GraphChiContext context)  {
        if (context.getIteration() == 0) {
            /* Initialize on first iteration */
            vertex.setValue(0.15f);
            
        } else {
            /* On other iterations, set my value to be the weighted
               average of my in-coming neighbors pageranks.
             */
            float sum = 0.f;
            for(int i=0; i<vertex.numInEdges(); i++) {
                sum += vertex.inEdge(i).getValue();
            }
            vertex.setValue(0.15f + 0.85f * sum);
        }

        /* Write my value (divided by my out-degree) to my out-edges so neighbors can read it. */
        float outValue = vertex.getValue() / vertex.numOutEdges();
        for(int i=0; i<vertex.numOutEdges(); i++) {
            vertex.outEdge(i).setValue(outValue);
        }

    }
	
	/**
     * Initialize the sharder-program.
     * @param graphName
     * @param numShards
     * @return
     * @throws IOException
     */
    protected static FastSharder createSharder(String graphName, int numShards) throws IOException {
        return new FastSharder<Float, Float>(graphName, numShards, new VertexProcessor<Float>() {
            public Float receiveVertexValue(int vertexId, String token) {
                return (token == null ? 0.0f : Float.parseFloat(token));
            }
        }, new EdgeProcessor<Float>() {
            public Float receiveEdge(int from, int to, String token) {
                return (token == null ? 0.0f : Float.parseFloat(token));
            }
        }, new FloatConverter(), new FloatConverter());
    }
    
	
    /**
     * Usage: java edu.cmu.graphchi.demo.PageRank graph-name num-shards filetype(edgelist|adjlist)
     * For specifying the number of shards, 20-50 million edges/shard is often a good configuration.
     */
    public static void main(String[] args) throws  Exception {
        String baseFilename = args[0];
        int nShards = Integer.parseInt(args[1]);
        numShards = nShards;
        int nSubPartitions = Integer.parseInt(args[2]);
        numSubPartition = nSubPartitions;
        String fileType = (args.length >= 4 ? args[3] : null);

        CompressedIO.disableCompression();

        /* Create shards */
        FastSharder sharder = createSharder(baseFilename, nShards);
        if (baseFilename.equals("pipein")) {     // Allow piping graph in
            sharder.shard(System.in, fileType);
        } else {
            if (!new File(ChiFilenames.getFilenameIntervals(baseFilename, nShards)).exists()) {
                sharder.shard(new FileInputStream(new File(baseFilename)), fileType);
            } else {
                logger.info("Found shards -- no need to preprocess");
            }
        }
        
        if(!new File(GraphQFileNames.getFileNameAbstractionGraph(baseFilename, nShards, nSubPartitions)).exists()) {
        	QueryPreProcessing<Float, Float> preProcessing = new QueryPreProcessing<Float, Float>(baseFilename, nShards, nSubPartitions);
            preProcessing.setEdataConverter(new FloatConverter());
            preProcessing.setVertexDataConverter(new FloatConverter());
            preProcessing.setModifiesInedges(false);  // Important optimization
            preProcessing.process();
            
            logger.info("finish preprocessing. start to do initialization...");
            /* Run GraphChi iteration 0 for initialization */
            GraphChiEngine<Float, Float> engine = new GraphChiEngine<Float, Float>(baseFilename, nShards);
            engine.setEdataConverter(new FloatConverter());
            engine.setVertexDataConverter(new FloatConverter());
            engine.setModifiesInedges(false); // Important optimization

            engine.run(new Pagerank(), 1);
            logger.info("finish initialization.");

		} else {
			logger.info(" Found abstraction graph. No need to preprocess.");
		}
        
        /* Run GraphChi iteration 0 for initialization */
//        GraphChiEngine<Float, Float> engine = new GraphChiEngine<Float, Float>(baseFilename, nShards);
//        engine.setEdataConverter(new FloatConverter());
//        engine.setVertexDataConverter(new FloatConverter());
//        engine.setModifiesInedges(false); // Important optimization
//
//        engine.run(new Pagerank(), 1);
//        logger.info("finish initialization.");
        
        /* Run graphQ */
        GraphQEngine<Float, Float> qEngine = new GraphQEngine<Float, Float>(baseFilename, nShards, nSubPartitions);
        qEngine.setEdataConverter(new FloatConverter());
        qEngine.setVertexDataConverter(new FloatConverter());
        qEngine.setModifiesInedges(false);
        qEngine.run(new PageRankQuery());

        logger.info("Ready.");
        
        VertexIdTranslate trans = qEngine.getVertexIdTranslate();
        if(targetVertex != null)
        	System.out.println("target vertex : " + trans.backward(targetVertex.getId()));
        else
        	System.out.println("query is not answered.");
        
        /* Output results */
//        int i = 0;
//        VertexIdTranslate trans = qEngine.getVertexIdTranslate();
//        TreeSet<IdFloat> top100 = Toplist.topListFloat(baseFilename, qEngine.numVertices(), 100);
//        for(IdFloat vertexRank : top100) {
//            System.out.println(++i + ": " + trans.backward(vertexRank.getVertexId()) + " = " + vertexRank.getValue());
//        }

    }

}
